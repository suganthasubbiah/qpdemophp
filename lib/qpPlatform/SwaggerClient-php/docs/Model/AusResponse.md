# AusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_id** | **string** | Alphanumeric card id and can be up to 32 character. | [optional] 
**card_number** | **string** | Full card number. | [optional] 
**exp_date** | **string** | Card expiration Date in MMYY format. | [optional] 
**reason_code** | **string** | The type of Response Code. For all types, please refer to &lt;a href&#x3D;\&quot;/developer/api/reference#account-updater-server-response-code\&quot; target&#x3D;\&quot;_blank\&quot;&gt;Account Updater Server Response Code&lt;/a&gt; | [optional] 
**card_number_new** | **string** | New full card number. | [optional] 
**exp_date_new** | **string** | New card expiration date in MMYY format. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


