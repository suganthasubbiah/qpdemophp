<?php
include 'includes/header.php';
include 'includes/back.php';
?>

<div class="row text-large" align="center"> Process Payment</div>
<div class="row text-large" align="center">Demo - Checkout</div>
<div >&nbsp;</div>
<br>
<?php include 'includes/cart.php'?>
<?php
require_once __DIR__ . '/lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';
?>

<?php

include 'includes/property.php';

/**
 * A sample to demonstrate Qualpay hosted checkout usage
 */

//Read property file
$mode = $transaction_mode;
$securityKey = $security_key;
$merchantId = $merchant_id;
$purchaseId = $purchase_id;
$qp_url = $url;

if (isset($_POST['submit'])) {

    echo "Submitting form";
    $first_name = $_POST['customer_first_name'];
    $last_name = $_POST['customer_last_name'];
    $billing_addr1 = $_POST['billing_street_addr1'];
    $city = $_POST['billing_city'];
    $state = $_POST['billing_state'];
    $zip = $_POST['billing_zip'];
    $country = $_POST['billing_country'];
    $email = $_POST['customer_email'];

    // Configure API.
    $config = new qpPlatform\Configuration();
    $config->setUsername($securityKey)
        ->setHost($qp_url . '/platform');

    $http_client = new GuzzleHttp\Client();
    $api_instance = new qpPlatform\Api\QualpayCheckoutApi($http_client, $config);
    $body = new qpPlatform\Model\CheckoutRequest();

    /*
    Build checkout request.
    The checkout form will be pre-populated with the customer data
     */
    $body->setAmtTran($amt_tran) //Transaction Amount
        ->setTranCurrency("840") //Numeric Currency Code
        ->setPurchaseId($purchase_id) //Transaction Purchase ID
        //->setCustomerId("JOHNDOE")   //Set customer ID if you would like to create a new customer, or associated this transaction to an existing customer
        ->setCustomerFirstName($first_name)//Card holder First Name
        ->setCustomerLastName($last_name)//Card holder Last Name
        ->setCustomerEmail($email)//Card holder Email
        ->setBillingAddr1($billing_addr1) //AVS Address
        ->setBillingCity($city)
        ->setBillingState($state)
        ->setBillingZip($zip); //AVS Zip

    // Set preferences, will override any preference set under checkout settings for your merchant
    $pref = new \qpPlatform\Model\CheckoutPreferences();
    $pref->setExpireInSecs(300)
        ->setAllowPartialPayments(false);
    $body->setPreferences($pref);

    //Generate a link for this checkout
    $result = $api_instance->addCheckout($body);
    $link = $result->getData()->getCheckoutLink();

    //Redirect browser to the checkout link
    if (!empty($link)) {
        header('Location: ' . $link);
    } else {
        echo "Unable to take payment";
    }

}
?>
    <form id="demo-payment-form" class="qp-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <div>
    <?php include 'includes/address.php'?>
  </div>
  <div id="continue-button" class="grid-full" align="center" style="display:block">
    <button type="button" class="btn btn-primary" onclick="showHideElement('payment'); showHideElement('continue-button')">Continue</button>
  </div>
  <div id="payment" class="row" style="display:none">
    <?php include 'includes/price.php'?>
    <div class="row">&nbsp;</div>
    <div class="row">
      <div class="grid-full" align="center">
        <input class="btn btn-primary" type="submit" name="submit" value="Pay Now"></input>
      </div>
    </div>
  </div>
</form>
<?php
include 'includes/footer.php';
?>