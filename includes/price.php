<div class="grid-sixth">&nbsp;</div>
<div class="grid-two-thirds">
  <div class="qp-box corner-all" style="margin-top: 10px;">
    <div class="row"><div class="grid-full">&nbsp;</div></div>
    <div class="row">
      <div class="grid-fourth">&nbsp;</div>
      <div class="grid-half" align="center">
        <span class="text-large">
        <?php
        echo '$' . round($amt_tran, 2, PHP_ROUND_HALF_UP);
        ?>
        &nbsp;&nbsp;
        </span>
        <input type="checkbox" name="show_price_detail" onclick="showHideElement('price-detail');"/> ShowDetails
      </div>
    </div>
    <div class="row" id="price-detail" style="display:none">
      <div class="grid-third">&nbsp;</div>
      <div class="qp-box corner-all grid-third">
        <div class="row x-small">
          <div class="grid-half">total: </div>
          <div align="right" class="grid-half">
          <?php echo '$' . round($total, 2, PHP_ROUND_HALF_UP); ?>
          </div>
        </div>
        <div class="row x-small"><div class="grid-half">tax: </div><div align="right" class="grid-half">
          <?php echo '$' . round($total * $amt_tax, 2, PHP_ROUND_HALF_UP);?>
        </div>
        </div>
        <div class="row x-small">
          <div class="grid-half">shipping: </div><div align="right" class="grid-half">
            <?php echo '$' . round($shipping, 2, PHP_ROUND_HALF_UP); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row"><div class="grid-full">&nbsp;</div></div>
  </div>
</div>
