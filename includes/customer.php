
  <div class="row">
    <div class="grid-sixth">&nbsp;</div>
    <div class="grid-two-thirds">
      <div class="qp-header corner-top" style="margin-top: 10px;">
        <div class="row">
          <div class="grid-full">Customer Information</div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="qp-box corner-bottom" style="margin-bottom: 10px;">
        <div class ="row">
          <div class="grid-half">
            <input type="text" name="customer_first_name" placeholder="First Name" value="John" maxlength=32 required ></input>
          </div>
          <div class="grid-half">
            <input type="text" name="customer_last_name" placeholder="Last Name" value="Doe" maxlength=32 required></input>
          </div>
        </div>
        <div class ="row">
          <div class="grid-half">
            <input type="email" name="customer_email_address" maxlength=64 placeholder="Email Address" </input>
          </div>
          <div class="grid-half">
            <input type="text" name="customer_phone" maxlength=16 placeholder="Phone Number"></input>
          </div>
        </div>
      </div>
    </div>
  </div>
