<?php

$transaction_mode = 'test';             //use 'live' for production mode

$url = 'https://api-test.qualpay.com'; //Use 'https://api.qualpay.com' for production mode

$security_key = '';                    // Use the production api security key for production mode

$merchant_id = '';                     //Use the production merchant id for live mode

$transaction_type = 'SALE';            //Or AUTH 

$purchase_id = 'Acme' . mt_rand(10000, 99999);
?>