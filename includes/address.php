<script>
  var showHideShipping = function () {
    var element = document.getElementById("billing");
    if (document.getElementById("billing_address_different").checked) {
      element.style.display = 'block';
    } else {
      element.style.display = 'none';
    }
  };
</script>
<div class="row">
  <div class="grid-sixth">&nbsp;</div>
  <div class="grid-two-thirds">
    <div class="qp-header corner-top" style="margin-top: 10px;">
      <div class="row">
        <div class="grid-full">Shipping Information</div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="qp-box corner-bottom" style="margin-bottom: 10px;">
      <div class="row">
        <div class="grid-half">
          <input type="text" name="shipping_first_name" placeholder="First Name"></input>
        </div>
        <div class="grid-half">
          <input type="text" name="shipping_last_name" placeholder="Last Name"></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-full">
          <input type="text" name="shipping_street_addr1" placeholder="Street Address"></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-fourth">
          <input type="text" name="shipping_city" placeholder="City"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="shipping_state" placeholder="State"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="shipping_zip" placeholder="Zip"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="shipping_country" placeholder="Country"></input>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="grid-sixth">&nbsp;</div>
  <div class="grid-two-thirds">
    &nbsp;&nbsp;<input type="checkbox" id="billing_address_different" name="billing_address_different" onclick="showHideElement('billing');" />
    Billing address is different from shipping
  </div>
</div>
<div class="row" id="billing" style="display:none">
  <div class="grid-sixth">&nbsp;</div>
  <div class="grid-two-thirds">
    <div class="qp-header corner-top" style="margin-top: 10px;">
      <div class="row">
        <div class="grid-full">Billing Information</div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="qp-box corner-bottom" style="margin-bottom: 10px;">
      <div class="row">
        <div class="grid-half">
          <input type="text" name="customer_first_name" placeholder="First Name" value="John" </input> </div> <div
            class="grid-half">
          <input type="text" name="customer_last_name" placeholder="Last Name" value="Doe"></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-full">
          <input type="text" name="billing_street_addr1" placeholder="Street Address" value="4th Avenue"></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-fourth">
          <input type="text" name="billing_city" placeholder="City" value="San Mateo"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_state" placeholder="State" value="CA"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_zip" placeholder="Zip" value="95054"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_country" placeholder="Country" value="USA"></input>
        </div>
      </div>
    </div>
  </div>
</div>