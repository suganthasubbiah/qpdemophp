<table class="qp-table" width="100%" align="center">
  <?php
   $items = '[
      {"image": "images/kitten2.jpg", "product" : "Kitten", "option" : "White", "quantity" : 1, "cost" : 45.98  },
      {"image": "images/kitten1.jpg", "product" : "Kitten", "option" : "Tiger", "quantity" : 2, "cost" : 16.87  }
    ]';
  $items_arr =  json_decode($items);
  $count = count($items_arr);
  $total = 0;
  $shipping = 4.99;
  $amt_tax = 0.0857;
  $color = '#ffffff';
  $i = 0;
  echo '<thead><tr>';
  echo '<th scope="col">Product</th>';
  echo '<th scope="col">Option</th>';
  echo '<th scope="col">Quantity</th>';
  echo '<th scope="col">Price</th>';
  echo '<th scope="col">Total</th>';
  echo '</tr></thead>';
  echo '<tbody>';
  foreach($items_arr as $item) {
    echo '<tr>';
    echo '<td data-title="Product"><img height="60" width="60" src=' . $item->image .' alt=' . $item->product . '</></td>';
    echo '<td data-title="Option">' . $item->option . '</td>';
    echo '<td data-title="Quantity">' . $item->quantity . '</td>';
    echo '<td data-title="Cost">' . '$' . $item->cost . '</td>';
    echo '<td data-title="Quantity">' . '$' . $item->quantity * $item->cost . '</td>';
    echo '</tr>';
    $total = $total + $item->cost * $item->quantity;
  }
  echo '<tr>';
  echo '<td>Total</td>';
  echo '<td>&nbsp;</td>';
  echo '<td>&nbsp;</td>';
  echo '<td>&nbsp;</td>';
  echo '<td>'. '$' . $total . '</td>';
  echo '</tr>';
  echo '</tbody>';
  $amt_tran = $total + $shipping + $total * $amt_tax;
   ?>
</table>
