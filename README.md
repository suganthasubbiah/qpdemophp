#README#
The purpose of this sample app is to demonstrate the usage of Qualpay products. 

**Dependencies**

You will need

* A qualpay sandbox account. Create your sandbox account at https://api-test.qualpay.com
* Ensure that you have an api security key and the key has access Payment Gateway API and Embedded Fields API. Refer to https://www.qualpay.com/developer/api/testing#security-keys
* A server to run this PHP web application
* Chrome or Firefox browser. The application has beeen tested on latest version of Chrome and Firefox.

**Configuration**

Update  includes/property.php

* Update merchant_id to point to your merchant account id.
* Udpate security_key to point to your merchant api security_key.
* Update any other property as appropriate.

**Code samples**

* services/payment-gateway.php - Invoke a sale/auth transaction with Qualpay payment gateway.
* embedded.php - Use Embedded fields to capture card data and use it to invoke a sale request.
* hosted-checkout.php - Use Qualpay Checkout API to generate a checkout link.
* services/customer-create.php - Create a customer vault record using Customer Vault API.
* services/subscription-create.php - Create a recurring payment for a customer using Recurring Billing API.

**Qualpay SDKs**

The Platform and Payment Gateway PHP SDKs enables you to connect to the Qualpay Platform and Payment Gateway REST APis. The SDKs can be downloaded from the developer portal https://www.qualpay.com/developer/libs/sdk

You can use composer (https://getcomposer.org/) to include the Qualpay dependencies in your project

To set up composer (These steps are to be repeated for each SDK that you include in your project)

* Download and copy the Qualpay SDK (platform or PaymentGateway) to your project library folder
* Change to the Qualpay SDK directory - SwaggerClient-php
* Download composer by following instructions detailed in the Download Composer section of https://getcomposer.org/download/ 
* Now run composer by running the following command from the same directory

```php composer.phar install```

You can now include the autload.php from the Qualpay sdk folders to use the Qualpay APIs. For example if the sdk is installed in the lib/qpPlatform folder on your project, then include the auload.php as follows

```<?php
require_once __DIR__ . '/lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';
?>```
