
<?php
require_once __DIR__ . '/../lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';
require_once __DIR__ . '/../lib/qpPg/SwaggerClient-php/vendor/autoload.php';

/**
 * A sample to demonstrate Payment Gateway API
 */

include('../includes/property.php');

define('MOTO_ECOMM_ID', 7);
define('DEV_ID', 'Qualpay_DemoV1.0');

//Read property file
$qp_url = $url;
$securityKey = $security_key;
$merchantId = $merchant_id;
$txn_type = $transaction_type;

//Form data
$first_name = $_POST['customer_first_name'];
$last_name = $_POST['customer_last_name'];
$billing_addr1 = $_POST['billing_street_addr1'];
$city = $_POST['billing_city'];
$state = $_POST['billing_state'];
$zip = $_POST['billing_zip'];
$country = $_POST['billing_country'];
$card_id = $_POST['card_id'];
$amount = $_POST['amt_tran'];
$card_number = $_POST['card_number'];

$customer_name = $first_name . ' ' . $last_name;

$zip = (strlen($zip) > 10) ? substr($zip, 0, 9) : $zip;
$customer_name = (strlen($customer_name) > 65) ? substr($customer_name, 0, 64) : $customer_name;

$moto_ecomm_ind = MOTO_ECOMM_ID;
$dev_id = DEV_ID;

$config = new \qpPg\Configuration();

$config->setUsername($securityKey)
    ->setHost($qp_url . "/pg");

// Invoke Payment gateway API
$http_client = new GuzzleHttp\client();
$api_instance = new \qpPg\Api\PaymentGatewayApi($http_client, $config);

//Build gateway request
$body = new \qpPg\Model\PGApiTransactionRequest();
$body->setAmtTran($amount) //Transaction Amount
    ->setMerchantId($merchantId) //Merchant ID
    ->setCardholderName($customer_name) //Card holder Name
    ->setTranCurrency('840') //Numeric Currency Code
    ->setAvsAddress($billing_addr1) //AVS Address
    ->setAvsZip($zip) //AVS Zip
    ->setPurchaseID($purchase_id) //Transaction Purchase ID
    ->setDeveloperId($dev_id) //Transaction developer id
    ->setMotoEcommInd($moto_ecomm_ind) //Ecommerce indicator. Default is 7
    ->setCardId($card_id); //card_id from embedded fields

//Invoke a payment transaction
try {
    if ($txn_type == 'SALE') {
        $result = $api_instance->sale($body);
    } else {
        $result = $api_instance->authorization($body);
    }
    $rcode = $result->getRcode();
    $pg_id = $result->getPgId();
    $msg = $result->getRmsg();
    echo $rcode;

} catch (Exception $e) {
    foreach ($e->getResponseBody() as $key => $value) {
        echo '$key => $value<br>';
        echo $msg = $e->getMessage();
        echo 'Exception when calling PaymentgatewayApi->authorization: ', $e->getMessage(), PHP_EOL;
    }
    echo 'Exception when calling PaymentgatewayApi->authorization: ', $e->getMessage(), PHP_EOL;
}
?>
