
<?php
require_once __DIR__ . '/../lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';

/**
 * Create a new customer and add a subscription to the customer
 */

include ('../includes/property.php');

define('MOTO_ECOMM_ID', 7);
define('DEV_ID', 'Qualpay_Drupal7');

//Read property file
$qp_url = $url;
$securityKey = $security_key;
$merchantId = $merchant_id;

//Form data
$customer_first_name = $_POST['customer_first_name'];
$customer_last_name = $_POST['customer_last_name'];
$customer_phone     = $_POST['customer_phone'];
$customer_email     = $_POST['customer_email_address'];

$billing_first_name = $_POST['billing_first_name'];
$billing_last_name = $_POST['billing_last_name'];
$billing_addr1 = $_POST['billing_street_addr1'];
$billing_city = $_POST['billing_city'];
$billing_state = $_POST['billing_state'];
$billing_zip = $_POST['billing_zip'];
$billing_country = $_POST['billing_country'];

$amt_setup = $_POST['amt_setup'];
$amt_tran = $_POST['amt_tran'];
$date_start = $_POST['date_start'];

$card_id = $_POST['card_id'];

$billing_zip = (strlen($billing_zip) > 10) ? substr($billing_zip, 0, 9) : $billing_zip;

$moto_ecomm_ind = MOTO_ECOMM_ID;
$dev_id = DEV_ID;

$config = new \qpPlatform\Configuration();

$config->setUsername($securityKey)
    ->setHost($qp_url . "/platform");

// Invoke Payment gateway API
$http_client = new GuzzleHttp\client();
$api_instance = new \qpPlatform\Api\RecurringBillingApi($http_client, $config);

//Build Customer record
$billing = new \qpPlatform\Model\AddBillingCardRequest();
$billing->setBillingFirstName($billing_first_name)
->setBillingLastName($billing_last_name)
->setBillingAddr1($billing_addr1)
->setBillingCity($billing_city)
->setBillingState($billing_state)
->setBillingZip($billing_zip)
->setBillingCountry($billing_country)
->setCardId($card_id);

$customer = new \qpPlatform\Model\AddCustomerRequest();
$customer->setAutoGenerateCustomerId(true)                 //Qualpay will auto generate customer id
     //->setCustomerId("JONDOE")                       //or set customer_id if you have your own id that you would like to use. Refer to documentation for allowed values in this field
     ->setCustomerFirstName($customer_first_name)      //Required
     ->setCustomerLastName($customer_last_name)        //Required
     ->setCustomerPhone($customer_phone)
     ->setCustomerEmail($customer_email)                  
    // ->setShippingAddresses(array($shipping))        
     ->setBillingCards(array($billing));               //Add billing information


//Build recurring billing request
$subscription = new \qpPlatform\Model\AddSubscriptionRequest();
$subscription->setCustomer($customer)                      //Create a new customer
           //->setCustomerId("JOHNDOE")                    // or use an existing customer id
             ->setDateStart($date_start)                    // Date subscription starts
           //->setPlanCode("plancode")                     //Set plan code if this is an on plan subscription. Else set off plan fields 
             ->setPlanDesc("An off plan subscription")     
             ->setPlanFrequency(3)                         //Monthly 
             ->setPlanDuration(10)                         //duration of the recurring charge
             ->setAmtSetup($amt_setup)                             //One time set up fee
             ->setAmtTran($amt_tran);                           //Monthly recurring charge

try {
    //Add subscription
    $result = $api_instance->addSubscription($subscription);
    $code = $result->getCode();
    $msg = $result->getMessage();
    $data = $result->getData();

    echo $result;
    http_response_code(201);
 
} catch (Exception $e) {
    http_response_code(503);
    echo $e->getResponseBody(); 
}
?>
