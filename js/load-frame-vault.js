(function ($) {
    qpEmbeddedForm.loadFrame(merchant_id, {
        formId: "demo-payment-form",                                      //Payment Form ID
        mode: mode,                                                       //Set to live for production
        transientKey: key,                                                //Embedded Transient key generated from the server
        tokenize: true,                                         //Set to true for storing token in card vault
        onSuccess: function (data) {                                        //On Success function handler
            const { card_id, card_number } = data;
            console.log("Token is ", card_id);
            let values = $("#demo-payment-form").serializeArray();
            //Capture card_id
            values = values.concat([
                { name: "card_id", value: card_id },
                { name: "card_number", value: card_number }
            ]);
            console.log("Values is ", values);
            //Add card_id to customer vault
            $.post("services/customer-create.php", values)
                .done(function (resp) { //data = rcode
                    let jsonResp = JSON.parse(resp);
                    alert("Customer " + jsonResp.data.customer_id + " created");
                    window.location.href = "vault.php";
                })
                .fail(function (error) {
                    alert("Error creating customer");
                    let msg = "";
                    let jsonResp = JSON.parse(error.responseText);
                    msg += "Cannot add customer: " + jsonResp.message;
                    if (jsonResp.data) {
                        jsonResp.data.forEach((key) => msg = msg + "\n" + jsonResp.data[key]);
                    }
                    alert(msg);
                    //embedded transient key would be expired here and the frame should be reloaded.
                    window.location.href = "recurring-billing.php";
                  });
        },
        onError: function (error) {                                         //On Error function handler
            let msg = "";
            if (error.detail) {
                error.detail.forEach((key) => msg = "\n" + error.detail[key]);
                alert("Error adding customer to vault. " + msg);
            }
        }
    }
    );
}
)(jQuery);


