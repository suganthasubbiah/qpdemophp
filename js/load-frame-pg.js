(function ($) {
    qpEmbeddedForm.loadFrame(merchant_id, {
        "formId": "demo-payment-form",                                       //Payment Form ID
        "mode": mode,                                                        //Set to live for production
        "transientKey": key,                                                 //Embedded Transient key generated from the server
        "tokenize": "false",                                                 //Set to true for storing token in card vault
        onSuccess: function (data) {                                         //On Success function handler
            const { card_id, card_number } = data;
            let amt_tran = document.getElementById("amt_tran").value;
            console.log("Token is ", card_id, amt_tran, purchase_id);
            let values = $("#demo-payment-form").serializeArray();
            //Capture card_id
            values = values.concat([
                { name: "card_id", value: card_id },
                { name: "card_number", value: card_number }
            ]);
            let query;
            //Use card_id in payment gateway request
            $.post("services/payment-gateway.php", values)
                .done(function (data) { //data = rcode
                    if (data && data.trim() === '000') {
                        query = 'amt=' + amt_tran + '&msg=' + 'Payment Approved';
                        window.location.href = "thankyou-page.php?" + query + "&page=receipt";
                    } else {
                        query = 'amt=' + amt_tran + '&msg=' + 'Check Card Details';
                        window.location.href = "error-page.php?" + query;
                    }
                });
        },
        onError: function (error) {                                         //On Error function handler
            let msg = "";
            if (error.detail) {
              error.detail.forEach((key) => msg = "\n" + error.detail[key]);
            }
            alert("Error processing the payment gateway request " + msg);
        }
    }
    );
}
)(jQuery);


