
<?php
include 'includes/header.php';
?>

<div class="row">
  <div class="grid-half"><a href="/qp_demo">Back to Dashboard</a></div>
  <div class="grid-half" align="right"><a class='pull-right' href="/qp_demo/preferences.php">Preferences</a></div>
</div>
<div class="row text-large" align="center"> Process Payment</div>
<div class="row text-large" align="center">Demo - Checkout Page</div>
<div >&nbsp;</div>

<?php include 'includes/cart.php'?>
<br>

<?php
$msg = $_GET['msg'];
$amount = $_GET['amt']
?>

<div ng-if="ctrl.payment.show_receipt" class="ng-scope" style="">
    <div class="row">
      <div class="grid-fourth">&nbsp;</div>
      <div class="grid-half">
        <div tran="ctrl.payment" class="ng-isolate-scope">
    <div align="center" class="text-large">
      Card Declined!
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
      <div class="grid-full receipt">
        <div class="row">&nbsp;</div>
        <div class="row">
          <div class="grid-fourth"><b>Invoice #:</b></div>
          <div class="grid-three-fourths ng-binding">Acme4187</div>
        </div>
        
        <div class="row">
          <div class="grid-fourth"><b>Amount:</b></div>
          <div class="grid-three-fourths ng-binding"><?php echo $amount; ?></div>
        </div>
        
        <div class="row">
          <div class="grid-fourth"><b>Message:</b></div>
          <div class="grid-three-fourths ng-binding"><?php echo $msg; ?></div>
        </div>
        <div class="row">&nbsp;</div>
      </div>
    </div>
</div>
      </div>
    </div>
  </div>
  <?php
include 'includes/footer.php';
?>
